//
//  AppState.swift
//  AppDemo
//
//  Created by Miha Hozjan on 01/12/2020.
//

import Foundation
import ComposableArchitecture
import ComposableCoreLocation

struct LocationID: Hashable { }

// MARK: - State

struct AppState: Equatable {

    struct A: Equatable { }
    struct B: Equatable {
        var userLat: Double = 0
        var userLon: Double = 0
    }

    var appName = "Location Demo"
    var stateA: A = A()
    var stateB: B = B()
}

// MARK: - Action

enum AppAction: Equatable {

    enum A: Equatable {
        case dummyAction
    }

    enum B: Equatable {
        case onAppear
        case location(LocationManager.Action)
    }

    case actionA(A)
    case actionB(B)
}

// MARK: - Environment

struct AppEnvironment {

    public var locationManager: LocationManager = .live
}

// MARK: - App Reducer

let appReducer = Reducer<AppState, AppAction, AppEnvironment>.combine(

    reducerA
        .pullback(
            state: \.stateA,
            action: /AppAction.actionA,
            environment: { $0 }
        ),

    reducerB
        .pullback(
            state: \.stateB,
            action: /AppAction.actionB,
            environment: { $0 }
        )
)

// MARK: - Reducer working on state `A`

private let reducerA = Reducer<AppState.A, AppAction.A, AppEnvironment> { state, action, environment in

    switch action {
    case .dummyAction:
        break
    }

    return .none
}

// MARK: - Reducer working on state `B`

private let reducerB = Reducer<AppState.B, AppAction.B, AppEnvironment> { state, action, environment in

    switch action {
    case .onAppear:
        return environment.locationManager
            .create(id: LocationID())
            .eraseToEffect()
            .map(AppAction.B.location)

    case .location:
        break
    }

    return .none
}
.combined(
    with:
        locationReducer
        .pullback(state: \.self, action: /AppAction.B.location, environment: { $0 })
)

private let locationReducer = Reducer<AppState.B, LocationManager.Action, AppEnvironment> { state, action, environment in

    switch action {
    case .didChangeAuthorization(.notDetermined):
        return environment.locationManager
            .requestAlwaysAuthorization(id: LocationID())
            .fireAndForget()

    case .didChangeAuthorization(.authorizedAlways),
         .didChangeAuthorization(.authorizedWhenInUse):
        return environment.locationManager
            .startUpdatingLocation(id: LocationID())
            .fireAndForget()

    case .didUpdateLocations(let locations):
        guard let last = locations.last else { break }
        state.userLat = last.coordinate.latitude
        state.userLon = last.coordinate.longitude

    default:
        break
    }

    return .none
}
