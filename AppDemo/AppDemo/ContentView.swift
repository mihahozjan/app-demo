//
//  ContentView.swift
//  AppDemo
//
//  Created by Miha Hozjan on 01/12/2020.
//

import SwiftUI
import MapKit
import ComposableArchitecture

struct ContentView: View {
    
    let store: Store<AppState, AppAction>

    var body: some View {
        VStack {
            WithViewStore(store.scope(state: { $0.appName })) { viewStore in
                Text(viewStore.state)
                    .onAppear(perform: {
                        viewStore.send(.actionB(.onAppear))
                    })
            }

            WithViewStore(store) { viewStore in
                MapView(
                    userLocation: CLLocationCoordinate2D(
                        latitude: viewStore.state.stateB.userLat,
                        longitude: viewStore.state.stateB.userLon
                    )
                )
            }
        }
    }
}

// MARK: - Map View Represntable

struct MapView: UIViewRepresentable {

    let userLocation: CLLocationCoordinate2D

    class Coordinator: NSObject, MKMapViewDelegate {
        let parent: MapView

        init(_ parent: MapView) {
            self.parent = parent
        }
    }

    func makeUIView(context: Context) -> MKMapView {
        let mapView = MKMapView()
        mapView.delegate = context.coordinator
        return mapView
    }

    func updateUIView(_ uiView: MKMapView, context: Context) {
        let region = MKCoordinateRegion(
            center: userLocation,
            latitudinalMeters: CLLocationDistance(exactly: 500)!,
            longitudinalMeters: CLLocationDistance(exactly: 500)!
        )

        uiView.setRegion(uiView.regionThatFits(region), animated: true)
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
}
