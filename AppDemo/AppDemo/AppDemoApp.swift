//
//  AppDemoApp.swift
//  AppDemo
//
//  Created by Miha Hozjan on 01/12/2020.
//

import SwiftUI
import ComposableArchitecture

@main
struct AppDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(
                store: Store(
                    initialState: AppState(),
                    reducer: appReducer.debug(),
                    environment: AppEnvironment()
                )
            )
        }
    }
}
